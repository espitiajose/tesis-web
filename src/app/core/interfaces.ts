
export interface User{
    objectId:string,
    username:string,
    email:string,
    password:string,
    firstName:string,
    lastName:string,
    address:string,
    phone:string
}


export interface Pagination{
    skip:number,
    count:number,
    current:number,
    pages: Array<number>
}