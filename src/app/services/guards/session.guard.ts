import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { AppService } from '../app.service';

@Injectable()
export class SessionGuard implements CanActivate, CanActivateChild {

  constructor(private _aS:AppService, private _r:Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      console.log(state);
      if(state.url === '/login' && this.verify()){
        this._r.navigate(["/app/dashboard"]);
        return false;
      }else{
        return true;
      }
  }

  canActivateChild(next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) : boolean {
    if(!this.verify()){
      this._r.navigate(["/login"]);
      return false;
    }else{
      return true;
    }
  }


  verify():boolean{
    console.log(this._aS.getUser());
    if(this._aS.getUser()){
      return true;
    }else{
      return false;
    }
  }
}
