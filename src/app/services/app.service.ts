import { Injectable } from '@angular/core';
import { User } from '../core/interfaces';
import { Storage } from '../core/storage';
declare var Parse:any;
Parse.initialize("AGLdLgc7bQmPzSssrbBQDzJJqeicVcLu4s4vwxGO", "HQ4sPXLRMBVBmAwzrJ2RPPUN5Sc5nP5TJ6IYoycL");
(Parse as any).serverURL = 'https://parseapi.back4app.com/';

@Injectable()
export class AppService {
  
  private numberPagination: Number = 10;
  private User = Parse.Object.extend("User");
  private Institution = Parse.Object.extend("Institution");
  private Faculty = Parse.Object.extend("Faculty");
  private Program = Parse.Object.extend("Program");
  private Event = Parse.Object.extend("Event");
  private UserProgram = Parse.Object.extend("UserProgram");
  private UserEvent = Parse.Object.extend("UserEvent");
  constructor() { }

  private tranformData(data: Array<any>, count:number, skip:number, includes:Array<string> = []){
    let newData: Array<any> = [];
    data.forEach(element => {
      let inf = {};
      if(element.id) inf["id"] = element.id;
      Object.keys(element.attributes).forEach((b)=>{
          inf[b] = includes.indexOf(b) == -1 ? element.attributes[b] : this.tranformObj(element.attributes[b]);
      });
      newData.push(inf);
    });
    return {data:newData, count: count, skip:skip};
  }

  tranformObj(data: any, includes:Array<string> = []):Object{
    let inf = {};
    if(data.id) inf["id"] = data.id; 
    Object.keys(data.attributes).forEach((b)=>{
      inf[b] = includes.indexOf(b) == -1 ? data.attributes[b] : this.tranformObj(data.attributes[b]);
    });
    return inf;
  }

  //USERS
  signIn(username:string, password:string): Promise<any>{
    return new Promise(async (resolve, reject) => {
      try {
        const user = await Parse.User.logIn(username, password);
        resolve(user);
      } catch (error) {
        reject(error);
      }   
    });
  }


  logOut():Promise<any>{
    return Parse.User.logOut();
  }

  getUser():any{
    return Parse.User.current();
  }

  getOneUser(id:string){
    return new Promise(async (resolve, reject) => {
      try {
        var query = new Parse.Query(this.User);
        let results = await query.get(id);
        resolve(this.tranformObj(results));
      }catch(error){
        reject(error);
      }
    });
  }

  getDataUser():User{
    return Storage.getAll('user') as User;
  }

  getUsers(skip:number=0, search:string = null){
    return new Promise(async (resolve, reject) => {
      try {
        let query = new Parse.Query(this.User);
        if(search) query.startsWith('firstName', search);
        let count = await query.count();
        let results = await query.skip(skip).limit(this.numberPagination).find();
        resolve(this.tranformData(results, count, skip));
      } catch (error) {
        reject(error);
      }   
    });
  }

  getCountUserStudent() {
    return new Promise(async (resolve, reject) => {
      try {
        let query = new Parse.Query(this.User);
        //query.equalTo("rols", '2');
        let count = await query.count();
        resolve(count);
      } catch (error) {
        reject(error);
      }
    });
  }

  getUserList(){
    return new Promise(async (resolve, reject) => {
      try {
        let query = new Parse.Query(this.User);
        let count = await query.count();
        let results = await query.find();
        resolve(this.tranformData(results, count, 0));
      } catch (error) {
        reject(error);
      }   
    });
  }

  createUser(dataUser:any): Promise<any>{
    var user = new Parse.User();
    user.set("username", dataUser.email);
    user.set("password", '123456');
    user.set("email", dataUser.email);
    user.set("phone", dataUser.phone);
    user.set("firstName", dataUser.firstName);
    user.set("lastName", dataUser.lastName);
    user.set("address", dataUser.address);
    user.set("rols", dataUser.rols);
    return new Promise(async (resolve, reject) => {
      try {
        await user.save();
        resolve(user);
      } catch (error) {
        reject(error);
      }   
    });
  }

  updateUser(dataUser:any){
    return new Promise(async (resolve, reject) => {
      try {
        var User = Parse.Object.extend("User");
        await new Parse.Query(User)
        .get(dataUser.id).then((user) => {
          user.set("username", dataUser.email);
          user.set("password", '123456');
          user.set("email", dataUser.email);
          user.set("phone", dataUser.phone);
          user.set("firstName", dataUser.firstName);
          user.set("lastName", dataUser.lastName);
          user.set("address", dataUser.address);
          user.set("rols", dataUser.rols);
          user.save();
          resolve(user);
        }, (error) => {
          reject(error);
        });
      }catch(error){
        reject(error);
      }
    });
  }

  // INSTITUTIONS

  getInstitutions(skip:number=0, search:string = null){
    return new Promise(async (resolve, reject) => {
      try {
        let query = new Parse.Query(this.Institution);
        if(search) query.startsWith('name', search);
        let count = await query.count();
        let results = await query.skip(skip).limit(this.numberPagination).find();
        resolve(this.tranformData(results, count, skip));
      } catch (error) {
        reject(error);
      }   
    });
  }

  getOneInstitution(id:string){
    return new Promise(async (resolve, reject) => {
      try {
        var query = new Parse.Query(this.Institution);
        let results = await query.get(id);
        resolve(this.tranformObj(results));
      }catch(error){
        reject(error);
      }
    });
  }

  getInstitutionList(){
    return new Promise(async (resolve, reject) => {
      try {
        let query = new Parse.Query(this.Institution);
        let count = await query.count();
        let results = await query.find();
        resolve(this.tranformData(results, count, 0));
      } catch (error) {
        reject(error);
      }   
    });
  }

  createInstitution(dataForm:any): Promise<any>{
    var data = new this.Institution();
    data.set("name", dataForm.name);
    data.set("address", dataForm.address);
    return new Promise(async (resolve, reject) => {
      try {
        await data.save();
        resolve(data);
      } catch (error) {
        reject(error);
      }   
    });
  }

  updateInstitution(dataForm:any){
    return new Promise(async (resolve, reject) => {
      try {
        await new Parse.Query(this.Institution)
        .get(dataForm.id).then((data) => {
          data.set("name", dataForm.name);
          data.set("address", dataForm.address);
          data.save();
          resolve(data);
        }, (error) => {
          reject(error);
        });
      }catch(error){
        reject(error);
      }
    });
  }

  // FACULTIES

  getFaculties(skip:number=0, search:string = null){
    return new Promise(async (resolve, reject) => {
      try {
        let query = new Parse.Query(this.Faculty);
        if(search) query.startsWith('name', search);
        let count = await query.count();
        let results = await query.skip(skip).limit(this.numberPagination).find();
        resolve(this.tranformData(results, count, skip));
      } catch (error) {
        reject(error);
      }   
    });
  }

  getFacultiesList(){
    return new Promise(async (resolve, reject) => {
      try {
        let query = new Parse.Query(this.Faculty);
        let count = await query.count();
        let results = await query.find();
        resolve(this.tranformData(results, count, 0));
      } catch (error) {
        reject(error);
      }   
    });
  }

  getOneFaculties(id:string){
    return new Promise(async (resolve, reject) => {
      try {
        var query = new Parse.Query(this.Faculty);
        let results = await query.get(id);
        resolve(this.tranformObj(results));
      }catch(error){
        reject(error);
      }
    });
  }

  createFaculties(dataForm:any): Promise<any>{
    var data = new this.Faculty();
    data.set("name", dataForm.name);
    return new Promise(async (resolve, reject) => {
      try {
        await data.save();
        resolve(data);
      } catch (error) {
        reject(error);
      }   
    });
  }

  updateFaculties(dataForm:any){
    return new Promise(async (resolve, reject) => {
      try {
        await new Parse.Query(this.Faculty)
        .get(dataForm.id).then((data) => {
          data.set("name", dataForm.name);
          data.save();
          resolve(data);
        }, (error) => {
          reject(error);
        });
      }catch(error){
        reject(error);
      }
    });
  }

  // programs

  getPrograms(skip:number=0, search:string = null){
    return new Promise(async (resolve, reject) => {
      try {
        let query = new Parse.Query(this.Program);
        query.include("faculty");
        if(search) query.startsWith('name', search);
        let count = await query.count();
        query.descending("createdAt");
        let results = await query.skip(skip).limit(this.numberPagination).find();
        resolve(this.tranformData(results, count, skip, ["faculty"]));
      } catch (error) {
        reject(error);
      }   
    });
  }

  getProgramsList(){
    return new Promise(async (resolve, reject) => {
      try {
        let query = new Parse.Query(this.Program);
        query.include("faculty");
        let count = await query.count();
        let results = await query.find();
        resolve(this.tranformData(results, count, 0, ["faculty"]));
      } catch (error) {
        reject(error);
      }   
    });
  }

  getOneProgram(id:string){
    return new Promise(async (resolve, reject) => {
      try {
        var query = new Parse.Query(this.Program);
        let results = await query.get(id);
        resolve(this.tranformObj(results));
      }catch(error){
        reject(error);
      }
    });
  }

  createProgram(dataForm:any): Promise<any>{
    var data = new this.Program();
    data.set("name", dataForm.name);
    var faculty = new this.Faculty();
    faculty.id = dataForm.faculty;
    data.set("faculty", faculty);
    return new Promise(async (resolve, reject) => {
      try {
        await data.save();
        resolve(data);
      } catch (error) {
        reject(error);
      }   
    });
  }

  updateProgram(dataForm:any){
    return new Promise(async (resolve, reject) => {
      try {
        await new Parse.Query(this.Program)
        .get(dataForm.id).then((data) => {
          data.set("name", dataForm.name);
          var faculty = new this.Faculty();
          faculty.id = dataForm.faculty;
          data.set("faculty", faculty);
          data.save();
          resolve(data);
        }, (error) => {
          reject(error);
        });
      }catch(error){
        reject(error);
      }
    });
  }

  // events

  getEvents(skip: number = 0) {
    return new Promise(async (resolve, reject) => {
      try {
        let query = new Parse.Query(this.Event);
        let count = await query.count();
        query.descending("createdAt");
        query.include("program");
        query.include("userRegister");
        let results = await query.skip(skip).limit(this.numberPagination).find();
        resolve(this.tranformData(results, count, skip, ["userRegister", "program"]));
      } catch (error) {
        reject(error);
      }
    });
  }

  getCountEventsAct() {
    return new Promise(async (resolve, reject) => {
      try {
        let query = new Parse.Query(this.Event);
        query.equalTo("approved", true);
        query.greaterThanOrEqualTo("start_date", new Date());
        let count = await query.count();
        resolve(count);
      } catch (error) {
        reject(error);
      }
    });
  }

  getCountEventsPending() {
    return new Promise(async (resolve, reject) => {
      try {
        let query = new Parse.Query(this.Event);
        query.equalTo("approved", false);
        query.greaterThanOrEqualTo("start_date", new Date());
        let count = await query.count();
        resolve(count);
      } catch (error) {
        reject(error);
      }
    });
  }

  getOneEvent(id: string) {
    return new Promise(async (resolve, reject) => {
      try {
        var query = new Parse.Query(this.Event);
        query.include("program");
        query.include("userRegister");
        let results = await query.get(id);
        resolve(this.tranformObj(results,["userRegister", "program"]));
      } catch (error) {
        reject(error);
      }
    });
  }

  getEventList(){
    return new Promise(async (resolve, reject) => {
      try {
        let query = new Parse.Query(this.Event);
        query.descending("createdAt");
        query.include("program");
        query.include("userRegister");
        let count = await query.count();
        let results = await query.find();
        resolve(this.tranformData(results, count, 0, ["userRegister", "program"]));
      } catch (error) {
        reject(error);
      }   
    });
  }

  createEvent(dataForm: any, file:File): Promise<any> {
    var data = new this.Event();
    data.set("name", dataForm.name);
    var program = new this.Program();
    program.id = dataForm.program;
    data.set("program", program);
    data.set("description", dataForm.description);
    data.set("place", dataForm.place);
    data.set("start_date", dataForm.start_date);
    data.set("end_date", dataForm.end_date);
    let user:any = this.tranformObj(this.getUser());
    if(user.rols.indexOf("1") != -1) data.set("approved", true);
    if(file){
      var parseFile = new Parse.File(file.name, file);
      data.set("image", parseFile);
    }
    return new Promise(async (resolve, reject) => {
      try {
        await data.save();
        resolve(data);
      } catch (error) {
        reject(error);
      }
    });
  }

  approvedEvent(id: string) {
    return new Promise(async (resolve, reject) => {
      try {
        await new Parse.Query(this.Event)
          .get(id).then((data) => {
            data.set("approved", true);
            data.save();
            resolve(data);
          }, (error) => {
            reject(error);
          });
      } catch (error) {
   
        reject(error);
      }
    });
  }

  disapproveEvent(id: string) {
    return new Promise(async (resolve, reject) => {
      try {
        await new Parse.Query(this.Event)
          .get(id).then((data) => {
            data.set("approved", false);
            data.save();
            resolve(data);
          }, (error) => {
            reject(error);
          });
      } catch (error) {
        reject(error);
      }
    });
  }

  updateEvent(dataForm: any, file:File) {
    return new Promise(async (resolve, reject) => {
      try {
        await new Parse.Query(this.Event)
          .get(dataForm.id).then((data) => {
            data.set("name", dataForm.name);
            data.set("description", dataForm.description);
            data.set("place", dataForm.place);
            data.set("start_date", dataForm.start_date);
            data.set("end_date", dataForm.end_date);
            if(file){
              var parseFile = new Parse.File(file.name, file);
              data.set("image", parseFile);
            }
            data.save();
            resolve(data);
          }, (error) => {
            reject(error);
          });
      } catch (error) {
        reject(error);
      }
    });
  }

  //dashboard

getReportUserProgram() {
  return new Promise(async (resolve, reject) => {
    try {
      let query = new Parse.Query(this.UserProgram);
      query.include("program");
      let count = await query.count();
      let results = await query.find();
      resolve(this.tranformData(results, count, 0, ["program"]));
    } catch (error) {
      reject(error);
    }
  });
}

getUsersAsp() {
  return new Promise(async (resolve, reject) => {
    try {
      let query = new Parse.Query(this.User);
      query.exists("institution");
      query.include("institution");
      let count = await query.count();
      let results = await query.find();
      resolve(this.tranformData(results, count, 0, ["institution"]));
    } catch (error) {
      reject(error);
    }
  });
}

getEventsAsp() {
  return new Promise(async (resolve, reject) => {
    try {
      let query = new Parse.Query(this.UserEvent);
      query.include("event");
      query.include("user");
      let count = await query.count();
      let results = await query.find();
      resolve(this.tranformData(results, count, 0, ["event", "user"]));
    } catch (error) {
      reject(error);
    }
  });
}
}




