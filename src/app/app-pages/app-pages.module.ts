import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { LayoutsModule } from './layouts/layouts.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { PAGES_ROUTES } from './app-pages.router';
import { AppPagesComponent } from './app-pages.component';
import { UsersComponent } from './pages/users/users.component';
import { UserFormComponent } from './pages/users/user-form/user-form.component';
import { AppService } from '../services/app.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { InstitutionsComponent } from './pages/institutions/institutions.component';
import { InstFormComponent } from './pages/institutions/inst-form/inst-form.component';
import { FacultiesComponent } from './pages/faculties/faculties.component';
import { FacFormComponent } from './pages/faculties/fac-form/fac-form.component';
import { ProgramsComponent } from './pages/programs/programs.component';
import { ProgFormComponent } from './pages/programs/prog-form/prog-form.component';
import { EventsComponent } from './pages/events/events.component';
import { EventFormComponent } from './pages/events/event-form/event-form.component';
import { NgxEditorModule } from 'ngx-editor';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { ChartsModule } from 'ng2-charts/ng2-charts';

@NgModule({
  declarations: [
      AppPagesComponent,
      DashboardComponent,
      UsersComponent,
      UserFormComponent,
      InstitutionsComponent,
      InstFormComponent,
      FacultiesComponent,
      FacFormComponent,
      ProgramsComponent,
      ProgFormComponent,
      EventsComponent,
      EventFormComponent
],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule,
    LayoutsModule,
    PAGES_ROUTES,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgxEditorModule,
    NgxMyDatePickerModule.forRoot(),
    ChartsModule
  ],
  providers: [AppService],
  bootstrap: []
})
export class AppPagesModule { }