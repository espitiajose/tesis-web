import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from 'src/app/services/app.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Storage } from '../../../../core/storage';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
declare var $:any;
import * as moment from 'moment';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.css']
})
export class EventFormComponent implements OnInit {

  forma: FormGroup;
  formSubmitAttempt: boolean = false;
  event: any;
  user: any;
  img:File;
  url:string;
  programs: Array<any> = [];
  myOptions: INgxMyDpOptions = {
    dateFormat: 'mm-dd-yyyy',
  };
  myOptions2: INgxMyDpOptions = {
    dateFormat: 'mm-dd-yyyy',
  };
  myOptionsText = {
    "editable": true,
    "spellcheck": true,
    "height": "200px",
    "minHeight": "auto",
    "width": "auto",
    "background": "#fff !important",
    "color": "black !important",
    "text-align": "left",
    "minWidth": "0",
    "z-index" : "-999999",
    "translate": "yes",
    "enableToolbar": true,
    "showToolbar": true,
    "placeholder": "Enter text here...",
    "imageEndPoint": "",
    "toolbar": [
        ["orderedList", "unorderedList", "horizontalLine"],
        ["bold", "italic", "underline", "strikeThrough"],
        ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
        ["cut", "copy", "delete", "removeFormat"],["undo", "redo"],
    ]

};


  constructor(private _aS: AppService,
    public _r: ActivatedRoute,
    private toast: ToastrService,
    private spinner: NgxSpinnerService) {
    this.forma = new FormGroup({
      name: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required),
      place: new FormControl(null, Validators.required),
      startDate: new FormControl(null, Validators.required),
      endDate: new FormControl(null, Validators.required),
      program: new FormControl("", Validators.required),
    });
  }

  ngOnInit() {
    let id = this._r.snapshot.paramMap.get('id');
    this.user = Storage.getAll('user');
    if (id) {
      this._aS.getOneEvent(id).then((res: any) => {
        this.event = res;
        this.forma.get('name').setValue(this.event.name);
        this.forma.get('description').setValue(this.event.description);
        this.forma.get('place').setValue(this.event.place);
        this.forma.get('startDate').setValue(this.dateTimeValue(this.event.start_date));
        this.forma.get('endDate').setValue(this.dateTimeValue(this.event.end_date));
      })
    }  
    this._aS.getProgramsList().then((res:any)=>{
      this.programs = res.data;
      if(id) this.forma.get('program').setValue(this.event.program.id);
    });
  }

  isFieldValid(field: string) {
    return (!this.forma.get(field).valid && this.forma.get(field).touched) ||
      (!this.forma.get(field).valid && this.formSubmitAttempt);
  }

  changeImage(file:File){
    this.img = file;
    let reader = new FileReader();
    reader.onload = function (e:any) {
      $("#image-load").css('background-image', 'url('+e.target.result+')');
      $("#image-load").css('background-repeat', 'no-repeat'); 
      $("#image-load").css('background-size', 'cover');
    }
    reader.readAsDataURL(file);
  }

  action() {
    this.formSubmitAttempt = true;
    if (this.forma.valid) {
      let values = this.forma.value;
      values['start_date'] = new Date(this.forma.get('startDate').value);
      values['end_date'] = new Date(this.forma.get('endDate').value);

      this.spinner.show();
      if (this.event) {
        values['id'] = this.event.id;
        values['userRegister'] = this.user.id;
        this._aS.updateEvent(values, this.img).then((res: any) => {
          this.toast.success('Evento actualizado exitosamente');
          this.spinner.hide();
          window.history.back();
        });
      } else {
        this._aS.createEvent(values, this.img).then((res: any) => {
          this.toast.success('Evento creado exitosamente');
          this.spinner.hide();
          window.history.back();
        });
      }
    }
  }

  private dateTimeValue(timeStamp){
      let date = moment(timeStamp).format('YYYY-MM-DD');
      let time = moment(timeStamp).format('HH:MM');
      return date+'T'+time;
  }

 

}
