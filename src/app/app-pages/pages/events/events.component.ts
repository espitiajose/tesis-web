import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../services/app.service';
import { Pagination } from '../../../core/interfaces';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ExcelService } from '../../../services/excel.service';
import * as moment from 'moment';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  events: Array<any> = [];
  event: any;
  usr: any;
  pagination: Pagination;
  user_admin:boolean = false;

  constructor(private _aS: AppService, private spinner: NgxSpinnerService, private toast: ToastrService, private _sE:ExcelService) { }

  ngOnInit() {
    this.getData();
    this.usr = this._aS.getUser();
    let obj:any = this._aS.tranformObj(this.usr);
    this.user_admin = obj.rols.indexOf("1") != -1;
  }

  getData(skip: number = 0) {
    this.spinner.show();
    this._aS.getEvents(skip).then((res: any) => {
      this.events = res.data;
      this.paginate(res);
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    })
  }

  Approved(id: string) {
    if (id) {
      this._aS.approvedEvent(id).then((res: any) => {
        this.toast.success('Evento aprobado exitosamente');
        this.spinner.hide();
        location.reload();
      });
    }
  }

  Disapprove(id: string) {
    if (id) {
      this._aS.disapproveEvent(id).then((res: any) => {
        this.toast.success('Evento desaprobado exitosamente');
        this.spinner.hide();
        location.reload();
      });
    }
  }

  paginate(res) {
    let end = Math.round(res.count / 10);
    let pages: Array<number> = [];
    for (let i = 0; i < end; i++) {
      pages.push(i + 1);
    }
    this.pagination = {
      count: res.count,
      skip: res.skip,
      current: res.skip / 10,
      pages: pages
    };
  }

  navigate(page: number) {
    if (this.pagination.current + 1 != page) {
      this.getData(page * 10);
    }
  }


  report(){
    this.spinner.show();
    this._aS.getEventList().then((res: any) => {
      let jsonExcel = [];
      res.data.forEach(el => {
        jsonExcel.push({
          'Id': el.id,
          'Aprobado': el.approved?"SI":"NO",
          'Lugar': el.place,
          'Programa': el.program ? el.program.name: "",
          'Registrado por': el.userRegister ? el.userRegister.firstName+" "+el.userRegister.lastName : "",
          'Fecha de inicio': moment(el.start_date).format("YYYY-MM-DD HH:MM"),
          'Fecha de finalización': moment(el.end_date).format("YYYY-MM-DD HH:MM"),
          'Url imagen': el.image ? el.image._url : "",
          'Fecha de creación': moment(el.createdAt).format("YYYY-MM-DD HH:MM"),
          'Última fecha de actualización': moment(el.updatedAt).format("YYYY-MM-DD HH:MM"),
        })
      });
      this._sE.exportAsExcelFile(jsonExcel, "Reporte_de_eventos_"+new Date().toString());
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    })
  }

}
