import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from '../../../../services/app.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
declare var $:any;

@Component({
  selector: 'fac-form',
  templateUrl: './fac-form.component.html',
  styleUrls: ['./fac-form.component.css']
})
export class FacFormComponent implements OnInit {

  forma:FormGroup;
  formSubmitAttempt:boolean = false;
  @Input() faculty:any;
  @Output() close = new EventEmitter();
  @Output() event = new EventEmitter();

  constructor(private _aS:AppService,
    private spinner: NgxSpinnerService,
    private toast:ToastrService) {
    this.forma = new FormGroup({
      name: new FormControl(null, Validators.required),
    });
  }

  ngOnInit() {
    $('#modalFaculty').on('hidden.bs.modal', (e) => { 
      this.forma.reset(); 
      this.close.emit();
    });
  }

  isFieldValid(field: string) {
    return (!this.forma.get(field).valid && this.forma.get(field).touched) ||
      (!this.forma.get(field).valid && this.formSubmitAttempt);
  }

  action(){
    this.formSubmitAttempt = true;
    if(this.forma.valid){
      this.spinner.show();
      if(this.faculty) {
        let values = this.forma.value;
        values['id'] = this.faculty.id;
        this._aS.updateFaculties(values).then((res:any)=>{
          this.toast.success('Facultad actualizada exitosamente');
          $('#modalFaculty').modal('hide');
          this.event.emit(res);
          this.spinner.hide();
          this.forma.reset();
        });
      }else{
        this._aS.createFaculties(this.forma.value).then((res:any)=>{
          this.toast.success('Facultad creada exitosamente');
          $('#modalFaculty').modal('hide');
          this.event.emit(res);
          this.spinner.hide();
          this.forma.reset();
        });
      }
    }
  }

}
