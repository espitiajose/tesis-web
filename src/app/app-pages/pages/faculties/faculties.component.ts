import { Component, OnInit } from '@angular/core';
import { Pagination } from '../../../core/interfaces';
import { AppService } from '../../../services/app.service';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';
import { ExcelService } from '../../../services/excel.service';
declare var $:any;

@Component({
  selector: 'app-faculties',
  templateUrl: './faculties.component.html',
  styleUrls: ['./faculties.component.css']
})
export class FacultiesComponent implements OnInit {

  faculties:Array<any> = [];
  pagination:Pagination;
  facultySelected:any;
  searchText:string = "";

  constructor(private _aS:AppService,
    private spinner: NgxSpinnerService,
    private _sE:ExcelService) { }

ngOnInit() {
  this.getData();
}

getData(skip:number = 0, search:string = null){
  this.spinner.show();
  this._aS.getFaculties(skip, search).then(
    (res:any)=>{
      this.faculties = res.data;
      this.paginate(res);
      this.spinner.hide();
    }, error =>{
      this.spinner.hide();
    });
}

paginate(res){
  let end = Math.round(res.count / 10);
  let pages:Array<number> = [];
  for (let i = 0; i < end; i++) {
    pages.push(i+1);
  }
  this.pagination = {
  count: res.count,
  skip: res.skip,
  current: res.skip / 10,
  pages: pages
  };
}

navigate(page:number){
  if(this.pagination.current + 1 != page){
  this.getData(page * 10);
  }
}

action(faculty?:any){
  if(faculty) this.facultySelected = faculty;
  $('#modalFaculty').modal('show');
}

search(){
  if(this.searchText != ""){
    this.getData(0, this.searchText);
  }else{
    this.getData();
  }
}

report(){
  this.spinner.show();
  this._aS.getProgramsList().then((res: any) => {
    let jsonExcel = [];
    res.data.forEach(el => {
      jsonExcel.push({
        'Id': el.id,
        'Nombre': el.name,
        'Ubicación': el.address,
        'Fecha de creación': moment(el.createdAt).format("YYYY-MM-DD HH:MM"),
        'Última fecha de actualización': moment(el.updatedAt).format("YYYY-MM-DD HH:MM"),
      })
    });
    this._sE.exportAsExcelFile(jsonExcel, "Reporte_de_programas_"+new Date().toString());
    this.spinner.hide();
  }, error => {
    this.spinner.hide();
  })
}
}
