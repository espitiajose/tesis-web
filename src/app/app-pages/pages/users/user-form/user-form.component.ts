import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, AbstractControl } from '@angular/forms';
import { AppService } from '../../../../services/app.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { rols } from 'src/app/core/constatntes';
declare var $:any;

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  forma:FormGroup;
  formSubmitAttempt:boolean = false;
  rols = [];
  user:any;
  constructor(private _aS:AppService,
              public _r: ActivatedRoute,
              private toast:ToastrService,
              private spinner: NgxSpinnerService) {
    this.forma = new FormGroup({
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      address: new FormControl(null, Validators.required),
      phone: new FormControl(null, Validators.required),
      rols: new FormArray([], this.minLengthArray(1)),
    });
    this.rols = rols;
  }

  ngOnInit() {
    let id = this._r.snapshot.paramMap.get('id');
    if(id){
      this._aS.getOneUser(id).then((res:any)=>{
        this.user = res;
        this.forma.get('firstName').setValue(this.user.firstName);
        this.forma.get('lastName').setValue(this.user.lastName);
        this.forma.get('email').setValue(this.user.username);
        this.forma.get('address').setValue(this.user.address);
        this.forma.get('phone').setValue(this.user.phone);
        if(this.user.rols){
          const formArray: FormArray = this.forma.get('rols') as FormArray;
          for (let i = 0; i < this.rols.length; i++) {
            for (let j = 0; j < this.user.rols.length; j++) {
              if(this.rols[i].code == this.user.rols[i]){
                formArray.push(new FormControl(this.rols[i].code.toString()));
                $('#'+this.rols[i].name+''+this.rols[i].code).prop('checked', true);
              } 
            }
          }
        }else{

        }
      })
    }
  }

  minLengthArray(min: number) {
    return (c: AbstractControl): {[key: string]: any} => {
        if (c.value.length >= min)
            return null;

        return { 'minLengthArray': {valid: false }};
    }
}

  isFieldValid(field: string) {
    return (!this.forma.get(field).valid && this.forma.get(field).touched) ||
      (!this.forma.get(field).valid && this.formSubmitAttempt);
  }

  action(){
    this.formSubmitAttempt = true;
    if(this.forma.valid){
      this.spinner.show();
      if(this.user) {
        let values = this.forma.value;
        values['id'] = this.user.id;
        this._aS.updateUser(values).then((res:any)=>{
          this.toast.success('Usuario actualizado exitosamente');
          this.spinner.hide();
          window.history.back();
        });
      }else{
        this._aS.createUser(this.forma.value).then((res:any)=>{
          this.toast.success('Usuario creado exitosamente');
          this.spinner.hide();
          window.history.back();
        });
      }
    }
  }

  onCheckChange(event) {
    const formArray: FormArray = this.forma.get('rols') as FormArray;
    if(event.target.checked){
      formArray.push(new FormControl(event.target.value));
    }
    else{
      let i: number = 0;
      formArray.controls.forEach((ctrl: FormControl) => {
        if(ctrl.value == event.target.value) {
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  

}
