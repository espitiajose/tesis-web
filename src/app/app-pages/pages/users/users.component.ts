import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../services/app.service';
import { User, Pagination } from '../../../core/interfaces';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';
import { ExcelService } from '../../../services/excel.service';
import { rols } from '../../../core/constatntes';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users:Array<any> = [];
  pagination:Pagination;
  searchText:string = "";
  
  constructor(private _aS:AppService, private spinner:NgxSpinnerService,
    private _sE:ExcelService) { }

  ngOnInit() {
    this.getData();
  }

  getData(skip:number = 0, search:string = null){
    this.spinner.show();
    this._aS.getUsers(skip, search).then((res:any)=>{
      this.users = res.data;
      this.paginate(res);
      this.spinner.hide();
    }, error =>{
      this.spinner.hide();
    })
  }

  paginate(res){
    let end = Math.round(res.count / 10);
    let pages:Array<number> = [];
    for (let i = 0; i < end; i++) {
      pages.push(i+1);
    }
    this.pagination = {
      count: res.count,
      skip: res.skip,
      current: res.skip / 10,
      pages: pages
    };
  }


  navigate(page:number){
    if(this.pagination.current + 1 != page){
      this.getData(page * 10);
    }
  }

  search(){
    if(this.searchText != ""){
      this.getData(0, this.searchText);
    }else{
      this.getData();
    }
  }

  report(){
    this.spinner.show();
    this._aS.getUserList().then((res: any) => {
      let jsonExcel = [];
      res.data.forEach(el => {
        jsonExcel.push({
          'Id': el.id,
          'Correo': el.username,
          'Nombre': el.firstName+' '+el.lastName,
          'Télefono': el.phone,
          'Dirección': el.address,
          'Roles': this.getRols(el.rols),
          'Fecha de creación': moment(el.createdAt).format("YYYY-MM-DD HH:MM"),
          'Última fecha de actualización': moment(el.updatedAt).format("YYYY-MM-DD HH:MM"),
        })
      });
      this._sE.exportAsExcelFile(jsonExcel, "Reporte_de_usuarios_"+new Date().toString());
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    })
  }

   getRols(rolsA:Array<any> = []):string{
    let str = "";
    for (let i = 0; i < rolsA.length; i++) {
      const element = rolsA[i];
      str+= " "+rols[Number(element) - 1].name;
    }
    return str;
  }

}
