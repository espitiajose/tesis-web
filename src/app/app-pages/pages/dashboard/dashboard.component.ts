import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../services/app.service';
import { ChartDataSets } from 'chart.js';
import { ExcelService } from '../../../services/excel.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  targets = {
    events: 0,
    pending_events: 0,
    students: 0,
    pending_request: 0,
  };
  lineChartType = 'bar';
  lineChartData: ChartDataSets[] = [
    {data: [], label: 'Programas'},
  ];
  lineChartLabels = [];
  dataAspInst = [];
  dataEventAsp = [];

  constructor(private _aS:AppService,
              private spinner: NgxSpinnerService,
              private _eS:ExcelService) { }

  ngOnInit() {
    this.inf_targets();
    this.data_chart();
    this.data_tables();
  }

  downloadReports(i:number){
    this.spinner.show();
    
    let jsonExcel = [];
    switch(i){
      case 1:
        //programas con mas interes
        this._aS.getReportUserProgram().then((res:any)=>{
          let labels = []; 
          let data = []; 
          for (let i = 0; i < res.data.length; i++) {
            const el = res.data[i];
            let idxLabel = labels.indexOf(el.program.name);
            if(idxLabel == -1){
              labels.push(el.program.name);
              data.push(1);
            }else{
              data[idxLabel] += 1 
            }  
          }
          labels.forEach((label, index) => {
            jsonExcel.push({
              'Progama': label,
              'Personas interesadas': data[index],
            })
          });
          this._eS.exportAsExcelFile(jsonExcel, "report_1_"+new Date().toString());
          this.spinner.hide();
        });
        break;
        case 2:
        //asp - colegio
        this._aS.getUsersAsp().then((res:any)=>{
          res.data.forEach(el => {
            jsonExcel.push({
              'Aspirante id': el.id,
              'Insitucion id': el.institution.id,
              'Nombre': el.firstName + ' ' + el.lastName,
              'Insitucion': el.institution.name,
              'Email': el.email,
              'Télefono': el.phone,
              'Dirección': el.address,
              'Fecha de nac': el.year_birth
            });
          });
          this._eS.exportAsExcelFile(jsonExcel, "report_2_"+new Date().toString());
          this.spinner.hide();
        });
        break;
        case 3:
        //asp - eventos
        this._aS.getEventsAsp().then((res:any)=>{
          let data = [];
          for (let i = 0; i < res.data.length; i++) {
            const el = res.data[i];
            if(el.event && new Date(el.event.start_date) >= new Date()){
              let idx = data.indexOf(e => e.event.objectId == el.event.objectId);
              if(idx == -1){
                data.push({event: el.event, aspirantes: [el.user]});
              }else{
                data[idx].aspirantes.push(el.user);
              }
            }  
          }
          console.log(data)
          data.forEach(da => {
            da.aspirantes.forEach(el => {
              jsonExcel.push({
                'Evento id': da.event.id,
                'Nombre del Evento': da.event.name,
                'Aspirante id': el.id,
                'Nombre': el.firstName + ' ' + el.lastName,
                'Email': el.email,
                'Télefono': el.phone,
                'Dirección': el.address,
                'Fecha de nac': el.year_birth
              });
            });
          });
          this._eS.exportAsExcelFile(jsonExcel, "report_3_"+new Date().toString());
          this.spinner.hide();
        });
        break;
    }
  }

  private inf_targets():void{
    this._aS.getCountEventsAct().then((res:any)=>{
      this.targets.events = res;
    });
    this._aS.getCountEventsPending().then((res:any)=>{
      this.targets.pending_events = res;
    });
    this._aS.getCountUserStudent().then((res:any)=>{
      this.targets.students = res;
    });
  }

  private data_chart():void{
    this._aS.getReportUserProgram().then((res:any)=>{
      let labels = []; 
      let data = []; 
      for (let i = 0; i < res.data.length; i++) {
        const el = res.data[i];
        let idxLabel = labels.indexOf(el.program.name);
        if(idxLabel == -1){
          labels.push(el.program.name);
          data.push(1);
        }else{
          data[idxLabel] += 1 
        }  
      }
      this.lineChartLabels = labels;
      this.lineChartData[0].data = data.length > 8 ? data.splice(7, data.length - 1) : data;
    });
  }

  private data_tables():void{
    this._aS.getUsersAsp().then((res:any)=>{
      this.dataAspInst = res.data;
    });
    this._aS.getEventsAsp().then((res:any)=>{
      let data = [];
      for (let i = 0; i < res.data.length; i++) {
        const el = res.data[i];
        if(el.event && new Date(el.event.start_date) >= new Date()){
          let idx = data.indexOf(e => e.event.id == el.event.id);
          if(idx == -1){
            data.push({event: el.event, aspirantes: [el.user]});
          }else{
            data[idx].aspirantes.push(el.user);
          }
        }  
      }
      this.dataEventAsp = data;
    });
  }



}
