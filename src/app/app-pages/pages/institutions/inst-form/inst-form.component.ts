import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from '../../../../services/app.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-inst-form',
  templateUrl: './inst-form.component.html',
  styleUrls: ['./inst-form.component.css']
})
export class InstFormComponent implements OnInit {

  forma:FormGroup;
  formSubmitAttempt:boolean = false;
  institution:any;

constructor(private _aS:AppService,
            public _r: ActivatedRoute,
            private toast:ToastrService,
            private spinner: NgxSpinnerService) {
this.forma = new FormGroup({
  name: new FormControl(null, Validators.required),
  address: new FormControl(null, Validators.required),
});
}

  ngOnInit() {
    let id = this._r.snapshot.paramMap.get('id');
    if(id){
      this._aS.getOneInstitution(id).then((res:any)=>{
        this.institution = res;
        this.forma.get('name').setValue(this.institution.name);
        this.forma.get('address').setValue(this.institution.address);
      })
    }
  }

  isFieldValid(field: string) {
    return (!this.forma.get(field).valid && this.forma.get(field).touched) ||
      (!this.forma.get(field).valid && this.formSubmitAttempt);
  }

  action(){
    this.formSubmitAttempt = true;
    if(this.forma.valid){
      this.spinner.show();
      if(this.institution) {
        let values = this.forma.value;
        values['id'] = this.institution.id;
        this._aS.updateInstitution(values).then((res:any)=>{
          this.toast.success('Institución actualizada exitosamente');
          this.spinner.hide();
          window.history.back();
        });
      }else{
        this._aS.createInstitution(this.forma.value).then((res:any)=>{
          this.toast.success('Institución creada exitosamente');
          this.spinner.hide();
          window.history.back();
        });
      }
    }
  }

  

}
