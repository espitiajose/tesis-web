import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from '../../../../services/app.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-prog-form',
  templateUrl: './prog-form.component.html',
  styleUrls: ['./prog-form.component.css']
})
export class ProgFormComponent implements OnInit {

  forma:FormGroup;
  formSubmitAttempt:boolean = false;
  faculties:Array<any> = [];
  program:any;

  constructor(private _aS:AppService,
    public _r: ActivatedRoute,
    private toast:ToastrService,
    private spinner: NgxSpinnerService) {
    this.forma = new FormGroup({
      name: new FormControl(null, Validators.required),
      faculty: new FormControl("", Validators.required),
    });
  }

  ngOnInit() {
    this._aS.getFacultiesList().then((res:any)=>{
      this.faculties = res.data;
      this.forma.get('faculty').setValue(this.program.faculty);
    })
    let id = this._r.snapshot.paramMap.get('id');
    if(id){
      this._aS.getOneProgram(id).then((res:any)=>{
        this.program = res;
        this.forma.get('name').setValue(this.program.name);
        this.forma.get('faculty').setValue(this.program.faculty);
      })
    }
  }

  isFieldValid(field: string) {
    return (!this.forma.get(field).valid && this.forma.get(field).touched) ||
      (!this.forma.get(field).valid && this.formSubmitAttempt);
  }

  action(){
    this.formSubmitAttempt = true;
    if(this.forma.valid){
      this.spinner.show();
      if(this.program) {
        let values = this.forma.value;
        values['id'] = this.program.id;
        this._aS.updateInstitution(values).then((res:any)=>{
          this.toast.success('Programa actualizado exitosamente');
          this.spinner.hide();
          window.history.back();
        });
      }else{
        this._aS.createProgram(this.forma.value).then((res:any)=>{
          this.toast.success('Programa creado exitosamente');
          this.spinner.hide();
          window.history.back();
        });
      }
    }
  }

}
