import { Routes, RouterModule } from '@angular/router';
import { AppPagesComponent } from './app-pages.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { SessionGuard } from '../services/guards/session.guard';
import { UsersComponent } from './pages/users/users.component';
import { UserFormComponent } from './pages/users/user-form/user-form.component';
import { InstitutionsComponent } from './pages/institutions/institutions.component';
import { InstFormComponent } from './pages/institutions/inst-form/inst-form.component';
import { FacultiesComponent } from './pages/faculties/faculties.component';
import { FacFormComponent } from './pages/faculties/fac-form/fac-form.component';
import { ProgramsComponent } from './pages/programs/programs.component';
import { ProgFormComponent } from './pages/programs/prog-form/prog-form.component';
import { EventsComponent } from './pages/events/events.component';
import { EventFormComponent } from './pages/events/event-form/event-form.component';


const pagesRoutes: Routes = [
    // { path: 'users', component: UserListComponent, data: { title: 'Dashboard', seccion: '1' } },
    {
      path: 'app',
      component: AppPagesComponent,
      canActivateChild: [ SessionGuard ],
      children: [
        { path: 'dashboard', component: DashboardComponent, data: { title: 'Dashboard', seccion: '1' } },
        { path: 'users', component: UsersComponent, data: { title: 'Usuarios', seccion: '2' } },
        { path: 'users/create', component: UserFormComponent, data: { title: 'Crear usuario'} },
        { path: 'users/update/:id', component: UserFormComponent, data: { title: 'Actualizar usuario'} },
        { path: 'institutions', component: InstitutionsComponent, data: { title: 'Instituciones', seccion: '3' } },
        { path: 'institutions/create', component: InstFormComponent, data: { title: 'Crear usuario'} },
        { path: 'institutions/update/:id', component: InstFormComponent, data: { title: 'Actualizar usuario'} },
        { path: 'faculties', component: FacultiesComponent, data: { title: 'Facultades', seccion: '3' } },
        { path: 'programs', component: ProgramsComponent, data: { title: 'Programas', seccion: '4' } },
        { path: 'programs/create', component: ProgFormComponent, data: { title: 'Crear programa'} },
        { path: 'programs/update/:id', component: ProgFormComponent, data: { title: 'Actualizar programa'} },
        { path: 'events', component: EventsComponent, data: { title: 'Eventos', seccion: '5' } },
        { path: 'events/create', component: EventFormComponent, data: { title: 'Crear evento'} },
        { path: 'events/update/:id', component: EventFormComponent, data: { title: 'Actualizar evento'} },
        { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      ]
    }
  ];
  export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );