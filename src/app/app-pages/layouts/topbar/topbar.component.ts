import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppService } from '../../../services/app.service';
import { Router } from '@angular/router';
import { User } from '../../../core/interfaces';
declare var $:any;

@Component({
  selector: 'topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  user:User;

  constructor(private _aS:AppService, private _r:Router) { }

  ngOnInit() {
    $('[role="dialog"]').appendTo('body');
    this.user = this._aS.getDataUser();
  }

  logout(){
    this._aS.logOut().then(data =>{
      $('#logoutModal').modal('hide');
      this._r.navigate(['login']);
    });
  }

}
