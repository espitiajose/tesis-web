import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../services/app.service';

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  nav_items = [
    { name: 'Usuarios',url: '/app/users',icon: 'fa-users' },
    { name: 'Instituciones',url: '/app/institutions',icon: 'fa fa-building'},
    { name: 'Facultades',url: '/app/faculties',icon: 'fa fa-pencil'},
    { name: 'Programas',url: '/app/programs',icon: 'fa fa-graduation-cap'},
    { name: 'Eventos',url: '/app/events',icon: 'fa fa-calendar'},
  ]

  action_bar_min:boolean = false;
  
  constructor(private _aS:AppService) {
    let user:any = _aS.tranformObj(_aS.getUser());
    if(user.rols.indexOf("3") != -1) this.nav_items.splice(1,3);
  }

  ngOnInit() {
  }

}
