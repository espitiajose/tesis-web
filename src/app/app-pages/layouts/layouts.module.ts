import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SidebarComponent } from './sidebar/sidebar.component'
import { TopbarComponent } from './topbar/topbar.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    SidebarComponent,
    TopbarComponent
  ],
  imports: [
    BrowserModule,
    RouterModule
  ],
  exports: [
    SidebarComponent,
    TopbarComponent
  ]
})
export class LayoutsModule { }