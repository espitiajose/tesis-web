import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../../services/app.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Storage } from '../../core/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  forma:FormGroup;
  error_login:boolean = false;

  constructor(private router:Router, private _aS:AppService) {
    this.forma = new FormGroup({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null),
      rememberme: new FormControl(false)
    });
  }

  ngOnInit() {
  }

  login(){
    this._aS.signIn(this.forma.get('username').value, this.forma.get('password').value)
    .then((res:any)=>{
      let user:any = this._aS.tranformObj(res);
      if(user.rols.indexOf("1") != -1 || user.rols.indexOf("3") != -1){
        Storage.setAll('user', user);
        this.router.navigate(['/app']);
      }else{
        this._aS.logOut();
        alert('no tienes permisos necesarios');
      }
    })
    .catch(error =>{
      this.error_login = true;
    })
  }

}
