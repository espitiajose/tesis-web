import {  Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { SessionGuard } from './services/guards/session.guard';

export const APP_ROUTES: Routes = [
    { path: 'login', component: LoginComponent, canActivate: [SessionGuard] },
    // { path: 'remember-password', component: RememberPasswordComponent },
    // { path: 'register', component: RegisterComponent },
    { path: 'page-not-found', component: NotFoundComponent },
    // { path: 'privacy-policy', component: PrivacyPolicyComponent },
    { path: '', redirectTo: 'login', pathMatch: 'full'},    
    { path: '**', component: NotFoundComponent },
];