import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { APP_ROUTES } from './app.router';
import { LoginComponent } from './pages/login/login.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { AppPagesModule } from './app-pages/app-pages.module';
import { AppService } from './services/app.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SessionGuard } from './services/guards/session.guard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxEditorModule } from 'ngx-editor';
import { HttpClientModule } from '@angular/common/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ExcelService } from './services/excel.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFontAwesomeModule,
    ToastrModule.forRoot({
      timeOut: 4000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
    RouterModule.forRoot( APP_ROUTES, {
      useHash: true,
      onSameUrlNavigation: 'reload'
    }),
    FormsModule,
    ReactiveFormsModule,
    AppPagesModule,
    NgxSpinnerModule,
    HttpClientModule,
    NgxEditorModule
  ],
  providers: [AppService, SessionGuard,ExcelService],
  bootstrap: [AppComponent]
})
export class AppModule { }
